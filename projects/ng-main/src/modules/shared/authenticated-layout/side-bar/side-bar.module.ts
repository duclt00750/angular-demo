import {NgModule} from '@angular/core';
import {SideBarComponent} from './side-bar.component';

@NgModule({
  declarations: [
    SideBarComponent
  ],
  exports: [
    SideBarComponent
  ]
})
export class SideBarModule {

}
